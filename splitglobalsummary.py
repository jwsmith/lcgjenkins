#!/usr/bin/env python

import os
import sys
import re
import argparse
import subprocess
import shutil
import urllib
import datetime
from string import Template, digits

def parse_summary_file(platform,version, build):

    today=datetime.datetime.now().strftime('%a') 
    if "nightly" in build:
        filecontent = urllib.urlopen('http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/nightlies/%s/%s/LCG_%s_%s.txt' %(version, today, version, platform)).read()
    else:    
        filecontent = urllib.urlopen('http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/releases/LCG_%s_%s.txt' %(version, platform)).read()

    package = {}
    offset = 3
    all_packages = []

    iterator = iter(filecontent.splitlines())
    iterator.next()
    packages_to_avoid = ""
    for line in iterator:
        if line[0] == '#' : 
            line_inputs = line.split(" ")
            packages_to_avoid = line_inputs[2]
            continue

        components = line.split(",")

        comp_version =  components[0].split()[2]
        
        if "SVNREVISION" in components[offset].split()[0] or "GIT" in components[offset].split()[0]:
            package['hash'] = components[offset+1].split()[1]
        else:
            package['hash'] = components[offset].split()[1]

        package['dest_directory'] = "./"+components[offset+2].split()[1]+"/"+components[offset+4].split()[1]+"/"+platform

        package['name'] = components[offset+3].split()[1]
        package['version'] = components[offset+4].split()[1]
        l =  components[offset+6:-1]
        l = [num.replace('DEPENDS:', "").strip().rsplit("-",5) for num in l]
        
        package['dependencies'] = [num[0]+"-"+num[-1] for num in l]
        
        for i in package['dependencies']:
            if packages_to_avoid in i:
                package['dependencies'].remove(i)
            l = ",".join(package['dependencies'])

        if not l:
            eachline = "%s; %s; %s; %s;" %(package['name'],package['hash'],package['version'],package['dest_directory'])
        else:
            eachline = "%s; %s; %s; %s; %s" %(package['name'],package['hash'],package['version'],package['dest_directory'],l)


        all_packages.append(eachline)

    return comp_version, all_packages


def getHeader(version, platform, compiler, comp_version):
    rpm_header = Template("""PLATFORM: $plat
VERSION: $version
COMPILER: $compiler;$comp_version
""").substitute(plat=platform,version=version,compiler=compiler,comp_version=comp_version)
    return rpm_header

if __name__ == "__main__":

    conts_mc = []
    conts_ext = []

    parameters = sys.argv
    version = parameters[1]
    platform = parameters[2]
    compiler = parameters[3]
    compiler = ''.join(c for c in compiler if c not in digits)
    build = parameters[4]

    comp_version, conts = parse_summary_file(platform, version, build)

    header = getHeader(version, platform, compiler, comp_version)

    file_name_mc = "LCG_generators_%s.%s" %(platform,"txt")
    file_name_ext = "LCG_externals_%s.%s" %(platform,"txt") 
    f_mc  = open(file_name_mc,"w") 
    f_ext  = open(file_name_ext,"w")
    f_mc.write(header) 
    f_ext.write(header)

    for lines in conts:
        if "MCGenerators" in lines:
            f_mc.write(lines + os.linesep)
        else:
            f_ext.write(lines + os.linesep)

    f_mc.close()
    f_ext.close()

# add the contrib file to 'thedir'                                                                                                                                                                                       
    shutil.copyfile("/cvmfs/sft.cern.ch/lcg/releases/LCG_contrib_%s.txt" %platform,"./LCG_contrib_%s.txt" %platform)
