#!/usr/bin/env python

import argparse

from releasepy import ReleaseTree

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('-r', '--release-number', help="Release number", default='auto', dest='releasever')
  parser.add_argument('-e', '--endsystem', help="installation check in CVMFS or AFS", default='AFS', dest='endsystem')
  parser.add_argument('-a', '--checkall', help="check packages for every installed platform", default=False, action='store_true', dest='checkall')
  parser.add_argument('-p', '--platform', help="platform to check", default='', dest='platform')

  args = parser.parse_args()

  print "Starting installation check..."

  release = ReleaseTree(args.endsystem, args.releasever)

  if args.checkall:
      release.checkRelease()
  elif args.platform:
      release.checkAllPackages(args.platform)
  else:
      raise RuntimeError("Platform to check or -a(--checkall to check all platforms) option must be specified")

if __name__ == "__main__":
  main()
