# -*- coding: utf-8 -*-

"""
releasepy.releasetree
~~~~~~~~~~~~~~~
This module parses a release directory and retrieves all its information
"""

import os
import utils
import paths
import buildinfo2json
import package
import glob
import shutil
import re
from collections import defaultdict, OrderedDict

# Difference of size allow (due to paths with different length)
threshold = 10000

class ReleaseTree(object):
    def __init__(self, endsystem, release_version):
        self.endsystem_path = paths.release_readonly(endsystem)
        self.release_name = self.getReleaseName(release_version)
        #self.platform = platform
        self.root_path = os.path.join(self.endsystem_path, self.release_name)
        self.release_version = release_version
        #self.allpackages = defaultdict(OrderedDict)
        self.allplatforms = defaultdict(OrderedDict)
        self.scanRelease()

    def getRootPath(self):
        return self.root_path

    def getReleaseName(self, release_version):
        # TODO: fix this
        if re.match(r'LCG_[a-zA-Z0-9]+', release_version):
            return release_version
        else:
            return "LCG_{0}".format(release_version)

    def scanPlatforms(self):
        regexStr=r'(x86_64|i686)-(slc\d|centos\d|cc\d|ubuntu[0-9]+|mac\d{4})-(gcc\d{2}|clang\d{2}|native|icc\d{2})-(opt|dbg)'
        dirs = set(glob.glob(os.path.join(self.root_path,'*/*/*')))
        dirs |= set(glob.glob(os.path.join(self.root_path, "MCGenerators/*/*/*")))
        dirs |= set(glob.glob(os.path.join(self.root_path, "Grid/*/*/*")))
        platforms = set([p.split("/")[-1] for p in dirs if re.match(regexStr,p.split("/")[-1])])
        return platforms

    def scanRelease(self):
        listOfPlatforms = self.scanPlatforms()
        for p in listOfPlatforms:
            self.scanPackages(p)

    def scanPackages(self,platform):
        allpackages = defaultdict(OrderedDict)
        dirs = glob.glob(os.path.join(self.root_path, '*/*', platform))
        dirs.extend(glob.glob(os.path.join(self.root_path, '*/*/*', platform)))
        for d in dirs:
            p = utils.splitall(d)
            if p[-4] == 'LCGCMT':
                continue
            name = p[-3]
            version = p[-2]
            directory = p[-4]
            try:
                try:
                    line = open(os.path.join(d, '.buildinfo_%s.txt' % name)).readlines()[0]
                except:
                    line = open(glob.glob(os.path.join(d, '.buildinfo_*.txt'))).readlines()[0]
                data = buildinfo2json.parse(line)
                # if d['NAME'] != d['DESTINATION']:
                #    print "# Skip package", d['NAME'], 'as it should be packaged in', d['DESTINATION']
                #    continue
                if 'DIRECTORY' not in line:
                    if 'MCGenerators' in directory or 'Grid' in directory:
                        data['DIRECTORY'] = directory+'/'+name
                    else:
                        data['DIRECTORY'] = name
                pkg = package.ExtendedPackage(name, data['VERSION'], data['HASH'], data['DIRECTORY'], data['DEPENDS'], platform, data['COMPILER'])
            except:
                #pkg = package.ExtendedPackage(name, data['VERSION'], data['HASH'], data['DIRECTORY'], [], platform, data['COMPILER'])
                if 'MCGenerators' in directory or 'Grid' in directory:
                    directory = directory+'/'+name
                else:
                    directory = name
                pkg = package.Package(name, version, directory, platform, '')
                #pass
            allpackages[pkg.getPackageName()][pkg.getVersion()] = pkg
        self.allplatforms[platform] = allpackages

    def getAllPackages(self, platform):
        allpackages = self.allplatforms[platform]
        list_of_lists = list([allpackages[x].values() for x in allpackages.keys()])
        flattened = [val for sublist in list_of_lists for val in sublist]
        return flattened

    def getListPackages(self, platform, pkgnames):
        allpackages = self.allplatforms[platform]
        list_of_lists = list([allpackages[x].values() for x in pkgnames])
        flattened = [val for sublist in list_of_lists for val in sublist]
        return flattened

    def getVersions(self, name, platform):
        return self.allplatforms[platform][name].values()

    def removePackage(self, name, version, platform, safeMode=True):
        allpackages = self.allplatforms[platform]
        if allpackages[name] and version in allpackages[name].keys():
            pkgRemove = allpackages[name][version]
        else:
            raise RuntimeError("The specified release doesn't contain package {0}-{1}".format(name,version))

        pkgRemoveFullPath = os.path.join(self.root_path, pkgRemove.getInstallPath())
        pkgDirectory = pkgRemove.getDirectory()
        if not os.path.exists(pkgRemoveFullPath):
            raise RuntimeError("Path doesn't exist: ", pkgRemoveFullPath)

        print "  Removing file :", pkgRemoveFullPath

        # Get the target path of the link
        targetPath = os.path.realpath(pkgRemoveFullPath)
        print "  Target of link: ", targetPath
        # Look for links of this package from every possible LCG_version release to targetPath
        expandedPaths = set(glob.glob(self.endsystem_path + "/LCG*/{0}/{1}/".format(pkgDirectory, version)))
        # Take into account package not previously considered MCGenerators or Grid package
        expandedPaths |= set(glob.glob(self.endsystem_path + "/LCG*/{0}/{1}/".format(pkgDirectory.replace('MCGenerators/', ''), version)))
        expandedPaths |= set(glob.glob(self.endsystem_path + "/LCG*/{0}/{1}/".format(pkgDirectory.replace('Grid/', ''), version)))
        existingLinks = utils.searchLinks(targetPath, expandedPaths)

        # Only remove package installed on /releases when the only link comes from the removed release
        if existingLinks == [pkgRemoveFullPath]:
            # Here would be the remove command
            print "  [!] Target of link will be removed, only one link (itself): ", "".join(existingLinks)
            #self.removefile.write('{0}\n'.format(searchPath))
            #self.removefile.flush()
            shutil.rmtree(targetPath)
            print "  Removed target path... ".format(targetPath)
        elif not existingLinks:
            print "  None links"
        else:
            print "  [v] Target of link WON'T BE REMOVED"
            print "  There are other {0} release(s) with links to this package:\n\t{1}".format(len(existingLinks)-1,
                    "\n\t".join(map(os.path.abspath, existingLinks)))
        # Now LCG_version/package/version/platform can be removed
        # Here would be the remove command
        os.unlink(pkgRemoveFullPath)
        print "  Removed {0}... ".format(pkgRemoveFullPath)
        #self.removefile.write('{0}\n'.format(pkgRemoveFullPath))

        # Check if version folder can be removed
        versionPath = os.path.join(self.root_path, pkgDirectory, version)
        platforms = os.listdir(versionPath)
        if not platforms:
            print "  Empty dir, removing ", versionPath
            #self.removefile.write('{0}\n'.format(versionPath))
            #self.removefile.flush()
            os.rmdir(versionPath)
        else:
            print "  Version directory still has {0} platform(s), won't be removed:\n\t{1}".format(len(platforms),
                    "\n\t".join([os.path.join(versionPath, p) for p in platforms]))

        # Check if package folder can be removed
        pkgPath = os.path.join(self.root_path, pkgDirectory)
        versions = os.listdir(pkgPath)
        if not versions:
            print "  Empty dir, removing ", pkgPath
            #self.removefile.write('{0}\n'.format(pkgPath))
            #self.removefile.flush()
            os.rmdir(pkgPath)
        else:
            print "  Package directory still have other {0} version(s), won't be removed:\n\t{1}".format(len(versions),
                    "\n\t".join([os.path.join(pkgPath,v) for v in versions]))

    def removeAllPackages(self, platform):
        allpackages = self.getAllPackages(platform)
        numpackages = len(allpackages)
        idx = 1
        for p in allpackages:
            print "[{0:03d} / {1:03d}] Start remove process for ".format(idx, numpackages), p.getName()

            self.removePackage(p.getPackageName(), p.getVersion(), platform)
            print "Finished."
            idx += 1

    def removeListPackages(self, platform, pkgnames):
        print "Removing list of packages: ", ", ".join(pkgnames)
        listpackages = self.getListPackages(platform, pkgnames)
        numpackages = len(listpackages)
        idx = 1
        for p in listpackages:
            print "[{0:03d} / {1:03d}] Start remove process for ".format(idx, numpackages), p.getName()

            self.removePackage(p.getPackageName(), p.getVersion(), platform)
            print "Finished."
            idx += 1

    def removeRelease(self):
        print "Removing all packages from: ", self.root_path
        # Remove packages
        for p in self.allplatforms.keys():
            self.removeAllPackages(p)

        # MCGenerators path
        generatorsPath = os.path.join(self.root_path, 'MCGenerators')
        removeCompiler = False
        if os.path.exists(generatorsPath):
            pkgs = os.listdir(generatorsPath)
            if not pkgs:
                print "  Removing MCGenerators: ", generatorsPath
                os.rmdir(os.path.join(self.removepath, 'MCGenerators'))
                removeCompiler = True
            else:
                print "  MCGenerators directory still have other {0} packages(s), won't be removed:\n\t{1}".format(len(pkgs),
                        "\n\t".join([os.path.join(generatorsPath, p) for p in pkgs]))

        # Grid path
        gridPath = os.path.join(self.root_path, 'Grid')
        if os.path.exists(gridPath):
            pkgs = os.listdir(gridPath)
            if not pkgs:
                print "  Removing Grid: ", gridPath
                os.rmdir(os.path.join(self.removepath, 'Grid'))
                removeCompiler = removeCompiler and True
            else:
                print "  Grid directory still have other {0} packages(s), won't be removed:\n\t{1}".format(len(pkgs),
                        "\n\t".join([os.path.join(gridPath, p) for p in pkgs]))

        # only remove compiler if MCGenerators and Grid have been removed too
        compilerPath = os.path.join(self.root_path, "gcc")
        if removeCompiler and os.path.exists(compilerPath):
            print "  Removing compiler: ", compilerPath
            shutil.rmtree(compilerPath)

        pkgs = os.listdir(self.root_path)
        if not pkgs:
            #self.removefile.write('{0}\n'.format(self.removepath))
            #self.removefile.flush()
            print "  Removing release directory: ", self.root_path
            os.rmdir(self.root_path)
        else:
            print "  Release directory still have other {0} packages(s), won't be removed:\n\t{1}".format(len(pkgs),
                    "\n\t".join([os.path.join(self.root_path, p) for p in pkgs]))

    def checkInstall(self, name, version, platform, debug=False):
        tmpdir = os.path.realpath("./.tmp/releasetree/checkInstall")
        if not os.path.exists(tmpdir):
            os.makedirs(tmpdir)
        p = self.allplatforms[platform][name][version]
        utils.unTAR(p, paths.release_tarfiles(), tmpdir)
        dirToCompare = os.path.join(tmpdir, p.getModifiedInstallPath())
        dirToCheck = os.path.realpath(os.path.join(self.root_path, p.getInstallPath()))

        # make the postinstallation to reproduce the same conditions
        postinstallfile = os.path.join(dirToCompare, '.post-install.sh')
        if os.path.exists(postinstallfile):
            utils.postinstall(postinstallfile, os.path.join(self.endsystem_path, p.getModifiedInstallPath()))

        sameInstall = utils.checkDirSize(dirToCheck, dirToCompare, threshold)

        # Only checks contents if size difference exceeds the threshold
        if not sameInstall:
            sameInstall = utils.checkDirContent(dirToCheck, dirToCompare, debug=False)

        # If successful, remove the untar file in tmpdir
        if sameInstall:
            rmdir = os.path.join(tmpdir, p.getDirectory())
            print "Removing temporal dir: ", rmdir
            shutil.rmtree(rmdir)
            return True
        else:
            return False

    def checkAllPackages(self, platform):
        failedPackages = []

        print "Checking installation path: ", self.root_path
        allpackages = self.getAllPackages(platform)
        numpackages = len(allpackages)
        idx = 1
        for p in allpackages:
            print "[{0:03d} / {1:03d}] Start checking process for ".format(idx, numpackages), p.getName()

            if not self.checkInstall(p.getPackageName(), p.getVersion(), platform):
                failedPackages.append(p)
            print "Finished."
            idx += 1

        print "{0} packages(s) may be wrong installed:\n\t{1}".format(len(failedPackages),
                "\n\t".join([p.getName() for p in failedPackages]))

    def checkRelease(self):
        print "=== Checking packages for every platform in ", self.root_path
        numplats = len(self.allplatforms.keys())
        idx = 1
        for p in self.allplatforms.keys():
            print "Platform [{0:03d} / {1:03d}]: ".format(idx, numplats), p
            self.checkAllPackages(p)
            idx += 1
