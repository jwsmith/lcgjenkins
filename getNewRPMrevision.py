#!/usr/bin/env python

# This script looks into existing RPMS and gives the lowest not used RPM revision

from os import listdir
from os.path import isfile, join, isdir
import shutil
import os
from optparse import OptionParser
import itertools
from common_parameters import officialarea

def get_new_RPM_revision(lcg_version,platform):
  # get the LCGMeta rpm revisions and print the highest one
  existingrpms = [ f.split("-")[2].split(".")[0] for f in list(itertools.chain(*map(lambda x: listdir(join(officialarea, x)), filter(lambda x: isdir(join(officialarea, x)), listdir(officialarea))))) if f.startswith("LCGMeta_%s_generators"%lcg_version) and platform.replace("-","_") in f]
  existingrpms.sort()
  if len(existingrpms)==0:
    print "ERROR: Cannot identify RPM revision!!"
    print "ERROR: Either release %s doesn't exist for %s" %(lcg_version, platform)
    print "ERROR: or the required LCGMeta_%s_generators... file has been lost" %lcg_version
    import sys, errno
    sys.exit(errno.EINVAL)  
  return int(existingrpms[-1])+1  

#########################
if __name__ == "__main__":
  # extract command line parameters
  usage = "usage: %prog lcg_version platform"
  parser = OptionParser(usage)
  (options, args) = parser.parse_args()    
  if len(args) != 2:
    parser.error("incorrect number of arguments.")
  else:
    lcg_version =  args[0] # lcg_version being built
    platform   =  args[1] # platform being built for

  print get_new_RPM_revision(lcg_version,platform)
