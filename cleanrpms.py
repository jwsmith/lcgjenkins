#!/usr/bin/env python

import sys, getopt, subprocess
import shutil
import collections
from collections import defaultdict
from glob import glob
# --------------------- Setting command lines options 
def main(argv):
   global build_type, op_sys, comp, build

   build_type = ''
   op_sys = ''
   build = ''
   comp = ''

   opts, args = getopt.getopt(argv,"hc:b:o:")
   for opt, arg in opts:
      if opt == '-h':
         print 'cleanrpms -c <compiler> -b <build_type> -o <operating_system>'
         sys.exit()
      elif opt in ("-c"):
         comp = arg

      elif opt in ("-b"):
         build = arg

      elif opt in ("-o"):
         op_sys = arg

   if build == 'Release' : build_type = 'opt'
   elif build == 'Debug' : build_type = 'dbg'
   elif build == 'Optimized' : build_type = 'opt'
   else : build_type = 'unk'

if __name__ == "__main__":

   main(sys.argv[1:])

   rpmlist = []


   filenames = glob('/afs/cern.ch/sw/lcg/external/rpms/lcg/*-*_*rpm')
   dest = "/afs/cern.ch/sw/lcg/external/rpms/rpms_old-1-20150722/"
   dd = defaultdict(list)

   for filename in filenames:
      if (filename.find(comp) != -1) and (filename.find(build_type) != -1) and (filename.find(op_sys) != -1):
         dd[tuple(filename.split('-1.0.0-')[:4])].append(filename)


   dc = collections.defaultdict(list)
   for key, value in dd:
      dc[key].append(value)
      
   for key, value in dc.iteritems():
      numerical = [i.split('.noarch.rpm', 1)[0] for i in value]
      T2 = eval(str(numerical).replace("'",""))
      maxnumber = max(T2)
      maxnumberstr = str(maxnumber)+".noarch.rpm"
      notmatching = [s for s in value if maxnumberstr not in s]
      for j in notmatching:
         rpms =  key+"-1.0.0-"+j
         rpmlist.append(rpms)

   for rpm_element in rpmlist:
      origin = rpm_element
      finaldestination = dest+rpm_element.rsplit('/', 1)[1]
      print "moving file", origin
      print "DESTINATION", finaldestination
      shutil.move(origin, finaldestination)
