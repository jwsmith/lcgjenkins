#!/usr/bin/env python
import datetime
import os,sys
import argparse
os.environ['DJANGO_SETTINGS_MODULE'] = "settings"
from django.core.exceptions import ObjectDoesNotExist

from relinfo.models import ReleasePackage, TagDependency, ReleaseTag, Package, Tag, Platform, ReleasePlatform, Release, LCG_RELEASE

def read_release_info(filename):
    # if it is a good json, just read it
    if filename.endswith("json"):
      return eval(file(filename).read())
    else:
      #here we have to parse the file
      pass

def fill_release_into_DB(release,date_input,description_input, remove_option):

    # check whether it already exists
    if len( Release.objects.filter(version = release["description"]["version"] )) != 0:
        if "YES" in remove_option:
            print "Based on your options, the Release %s is going to be removed" %release["description"]["version"]
            Release.objects.filter(version = release["description"]["version"]).delete()
            sys.exit()
        else:    
            print "WARNING: Release %s already exists" %release["description"]["version"]
            r_db = Release.objects.filter(version = release["description"]["version"])[0]

    else:
      r_db = Release(version = release["description"]["version"],
#                  description = "Setting the contains of the LCG_71root6 release",
#                  date = datetime.datetime.today(),# release["description"]["date"],
                  description = description_input,   
                  date = date_input,    
                  type = LCG_RELEASE)
      r_db.save()
    add_platform_to_release(r_db,release)

def fill_tag_dependencies(release):
    pass    

def add_platform_to_release(r_db,release):
    """add a platform and the corresponding tags"""
    # create platform if not there yet
    platform = release["description"]["platform"]
    result = Platform.objects.filter(name = platform)
    if len(result) == 0:
      p_db = Platform(name = platform)
      p_db.save()
    else:
      p_db = result[0] 
    print "adding the platform %s" %p_db
    # add platform to release if not already part of it
    result = r_db.platforms.filter(name=platform)
    if len(result) == 0:
      rp_db = ReleasePlatform(release = r_db, platform = p_db)
      rp_db.save()
    # finally start adding the package tags
    add_package(release["packages"],r_db,p_db)
    add_package_to_release(release["packages"],r_db,p_db)
    add_tags_to_release(release["packages"],r_db,p_db)
    add_tag_dependencies(release["packages"],r_db,p_db)

def add_package(packages,r_db,p_db):
    for package, info in packages.iteritems():
      if "-" in package:
        package = package[:-6]
      pkg=Package(name = info["name"])
      try:
       pkg.save()
      except BaseException:
       continue

def add_tags_to_release(packages,r_db,p_db):
    for package, info in packages.iteritems():
      if "-" in package:
        package = package[:-6]
      full_version = "%s-%s" %(info["version"],info["hash"]) #TODO: fix in case of empty hash
      full_version = info["version"]# full_version[:-6]
      try: 
#        print 'PACKAGE='+package+' VERSION='+full_version
#        pkg=Package(name = package,)        
        package = Package.objects.get(name = package)
        tag, created = Tag.objects.get_or_create(name = full_version, package = package)
        if created: 
          print "created %s of %s" %(full_version, package)  
        release_tag, created = ReleaseTag.objects.get_or_create(release=r_db, tag=tag, platform=p_db)
        if created:
          print "Attached tag %s of %s to release." %(full_version, package) 
      except ObjectDoesNotExist:
        print "Package %s does not exist" %package
        continue 

def add_package_to_release(packages,r_db,p_db):
    for package, info in packages.iteritems():
      package = package[:-6]
      pkg_name = info["name"]
      print pkg_name
      package = Package.objects.get(name = pkg_name)
      print package.id
      release_package, created = ReleasePackage.objects.get_or_create(release=r_db, package=package, platform=p_db)
      if created:
         print "Attached package %s of %s to release." %(pkg_name, package) 
#      except ObjectDoesNotExist:
#        print "Package %s does not exist" %package
#        continue 
#      except BaseException:
#        print "BaseException"
#        continue

def add_tag_dependencies(packages,r_db,p_db):
    #tags = 
    for package, info in packages.iteritems():
#     try:
      deps = info["dependencies"]
      fromtag=getTagByPkgNameAndVersion(info["name"],info["version"])

      for dep in deps:
       print dep
       for packagen, inf in packages.iteritems():
        full_version = "%s-%s" %(inf["name"],inf["hash"])
        if dep == full_version:
         totag=getTagByPkgNameAndVersion(inf["name"],inf["version"])
         tagr, created = TagDependency.objects.get_or_create(from_tag=fromtag, to_tag=totag, platform=p_db)
         if created: 
           print "created tagdependency" + str(tagr.id)   
#     except BaseException:
#       continue


def getTagByPkgNameAndVersion(pkgname,pkgversion):
    pak = Package.objects.get(name = pkgname)
    tag = Tag.objects.get(package=pak.id,name=pkgversion)
    return tag 

##########################
if __name__ == "__main__": 


  parser = argparse.ArgumentParser()
  parser.add_argument('-f', help="json file name", default='', dest='filename')
  parser.add_argument('-d', help="date of release YYYY-MM-DD", default='', dest='date_input')
  parser.add_argument('-e', help="brief explanation of the release", default='', dest='description_input')
  parser.add_argument('-o', help="Remove option YES/NO", default="NO", dest='remove_option')

  args = parser.parse_args()

  release = read_release_info(args.filename)

  fill_release_into_DB(release, args.date_input, args.description_input, args.remove_option)
