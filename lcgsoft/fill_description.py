#!/usr/bin/env python
import os,sys
os.environ['DJANGO_SETTINGS_MODULE'] = "settings"
from relinfo.models import License, Contact, Package, PackageContact
from relinfo.models import PACKAGE_CATEGORIES, PACKAGE_LANGUAGES
 
def index_from_enum(value,enum):
    """Fetch the ID from a Django type enum """
    result = None
    value = value.replace("Cpp","C++")
    for item in enum:
      if value in item[1]:
        result = item[0]
        return result
    raise IndexError("couldn't find %s" %value)

def read_package_info(filename):
    return eval(file(filename).read())

def fill_licenses_into_DB(packages):
    licenses = set()
    for package in packages:
      licenses.add(package["license"])
    for license in licenses:
      print "Filling %s" %license
      l = License(name = license, url="")
      try:
       l.save()
      except BaseException:
       continue

def fill_contacts_into_DB(packages):
     contacts = set()
     for package in packages:
      contact = package["contact"]
      if contact != "None" and contact != "":
        contacts.add(package["contact"])
        print 'FILLING CONTACT' + contact
     for contact in contacts:
       print "Filling %s" %contact
       email = contact.split()[-1]
       name = contact.split(email)[0]
       c = Contact(name=name, email=email )
       try:        
        c.save()
       except BaseException:
        continue

def fill_packages_into_DB(packages):
    for package in packages:
      try:
        email = package["contact"].split()[-1]
        c = Contact.objects.filter(email = email)[0]
        l = License.objects.filter(name = package["license"])[0]
        type = 0 if package["project"] == "true" else 1  
        category = index_from_enum(package["category"], PACKAGE_CATEGORIES)
      except IndexError:
        category = 8 #other category
      try:
       language = index_from_enum(package["language"], PACKAGE_LANGUAGES)
      except:
       pass
      try:
       p = Package(name = package["name"],
                  fullname = package["fullname"],
                  description = package["description"],
                  type = type,
                  category = category,
                  language = language,
                  homepage = package["homepage"],
       )
       print p.name
       p.save()
       print "ADDED "+p.name
      except:
       pass
      p = Package.objects.filter(name =package["name"])[0]
      p.license = l
      p.fullname = package["fullname"]
      p.type = type
      p.category = category
      p.language = language
      p.save()    
      pc = PackageContact(package = p, contact = c)      
      pc.save()
 
##########################
if __name__ == "__main__": 

  parameters = sys.argv
  if len(parameters) != 2:
    print "You have to provide a full path to packageinfo.txt\n python fill_description.py [full path to packageinfo.txt]"
    sys.exit()
  path = parameters[1]
  packages = read_package_info(path+"/packageinfo.txt")
  fill_contacts_into_DB(packages)
  fill_licenses_into_DB(packages)
  fill_packages_into_DB(packages)
