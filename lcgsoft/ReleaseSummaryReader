#!/usr/bin/env python

import datetime
import os,sys
from string import Template
from datetime import date
import calendar

def extract_package_info(entry):
    """Extract the package information of a line formed like this:
      xqilla; cefdd;  2.2.4p1; ./xqilla/2.2.4p1/x86_64-slc6-gcc48-dbg; XercesC-8ccd5
     """
    package = {}
    name, hash, version, directory, dependency_string = entry.split(";")
    package['name'] = name.strip()
    package['hash'] = hash.strip()
    package['version'] = version.strip()

    if "MCGenerators" in directory or "Grid" in directory:
        dest_name = directory.split("/")[2]
        destination_directory = directory.split('/', 3)
        dest_directory = destination_directory[1]+"/"+destination_directory[2]  
    else:
        dest_name= directory.split("/")[1]
        destination_directory = directory.split('/', 2)
        dest_directory = destination_directory[1]

    package['dest_name'] = dest_name
    package['dest_directory'] = dest_directory

    dependencies = [f.lstrip() for f in dependency_string.split(",") if f.lstrip()!=""]
    package["dependencies"] = dependencies
    return "%s-%s" %(package['name'],package['hash']), package

def num_there(s):
    return any(i.isdigit() for i in s)

def extract_package_info_from_buildinfo(filename):
    filecontent = file(filename).read()
    components = filecontent.split(",")

    offset = 3
    if "GITHASH" in filecontent and "DEPENDS" not in filecontent and "DIRECTORY" in filecontent:
        case=1

    elif "GITHASH" in filecontent and "DEPENDS" not in filecontent and "DIRECTORY" not in filecontent:
        case = 2
    elif "SVNREVISION" in filecontent:
        case=2
    elif "DEPENDS" in filecontent:
        case=3
    package = {}

    package['hash']     = components[offset].split()[1]
    package['dest_name'] = components[offset+1].split()[1]

    if "MCGenerators" in filename:
           package['dest_directory'] = "MCGenerators"+"/"+components[offset+1].split()[1]
    elif "Grid" in filename:
           package['dest_directory'] = "Grid"+"/"+components[offset+1].split()[1]
    else:
           package['dest_directory'] = components[offset+1].split()[1]

    if (case == 1):
        package['name'] = components[offset+3].split()[1]
        package['version'] = components[offset+4].split()[1]

        l =  components[offset+5:-1]
        l = [num.rsplit("-",5) for num in l]
        ll = [num[0]+"-"+num[-1] for num in l]
        package["dependencies"] =ll


    elif(case == 2):
        package['name'] = components[offset+2].split()[1]
        package['version'] = components[offset+3].split()[1]
        package["dependencies"] = components[offset+4:-1]

    elif(case == 3):
        package['name'] = components[offset+3].split()[1]
        package['version'] = components[offset+4].split()[1]
        l =  components[offset+6:-1]
        l = [i.replace('DEPENDS:', "") for i in l]
        l = [num.strip() for num in l]
        l = [num.rsplit("-",5) for num in l]
        ll = [num[0]+"-"+num[-1] for num in l] 
        package["dependencies"] =ll

    return "%s-%s" %(package['name'],package['hash']), package


def parse_summary_file(lcg_version,platform):
    distributions = ["externals","generators"]

    base = ' '

    packages_all = []
    for i in distributions:

        filename = os.path.join(base,"LCG_%s"%lcg_version,"LCG_%s_%s.txt" %(i,platform) )
        filecontent = file(filename).read()
        
        release = {}
        packages = {}
        release["description"] = {"version" : lcg_version, "platform" : platform}
        iterator = iter(filecontent.splitlines())
        iterator.next() # skip version line
        iterator.next() # skip platform line
        iterator.next() # skip compiler line VALID ONLY FROM 76root6
        for line in iterator:
            name_hash, package = extract_package_info(line)
            packages[name_hash] = package
            all_packages = packages.copy()
            all_packages.update(packages)

            
    return all_packages

def build_releaseinfo_from_installarea(lcg_version,platform,base,buildtype):
  if "release" in buildtype:    
      releasedir = os.path.join( base,"LCG_%s"%lcg_version )
  else:
      releasedir = base

  packages_dummy = []
  if "dummy" in platform:
      return packages_dummy

  releasedepth = releasedir.count("/")

  packages = {}
  # collect all .buildinfo_<name>.txt files
  for root, dirs, files in os.walk(releasedir,followlinks=True,topdown=True):
    if root.endswith(platform) or root.count("/") > (releasedepth+3):
        del dirs[:]
    for afile in files:
      if afile.endswith('.txt') and afile.startswith(".buildinfo") and platform in root:
        fullname = os.path.join(root, afile)
        name_hash, package = extract_package_info_from_buildinfo(fullname)
        packages[name_hash] = package
        all_packages = packages.copy()
        all_packages.update(packages)

  return all_packages     


def getHeader(lcg_version,platform, conts):
    rpm_header = Template("""                                                                       
{
'description' :{
 'version' : '$version',
 'platform': '$plat'
 
},
'packages': $contains
}
""").substitute(version=lcg_version,plat=platform,contains=conts)
    
    return rpm_header
                                  

##########################
if __name__ == "__main__":

    parameters = sys.argv

    my_date = date.today()
    today = calendar.day_name[my_date.weekday()][0:3]

    lcg_version = parameters[1]
    platform = parameters[2]
    buildtype = parameters[3] 

    if "release" in buildtype: 
        base = "/cvmfs/sft.cern.ch/lcg/releases"
    else:
        base = "/cvmfs/sft.cern.ch/lcg/nightlies/%s/%s/" %(lcg_version,today)

    print base    
    conts = build_releaseinfo_from_installarea(lcg_version,platform,base,buildtype)

    file_contains = getHeader(lcg_version,platform, conts)
    file_name = "%s-%s.%s" %(lcg_version,platform,"json")
    
    f  = open(file_name,"w") 
    f.write(file_contains)
    f.close() 
